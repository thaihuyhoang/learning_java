import java.util.Scanner;
public class DemoScanner {
	public static void main (String[] args) {
		//khai bao doi tuong (name, age)
		Scanner name = new Scanner(System.in);
		Scanner age = new Scanner(System.in);
	
		//khai bao bien YourName
		System.out.print("Your name: ");
		String YourName = name.nextLine();
		
		//khai bao bien YourAge
		System.out.print("Your age: ");
		int YourAge = age.nextInt();
		
		System.out.print("Anh " +YourName + " êi,"+" năm nay bạn đã " + YourAge + " tuổi rồi.");
	}
}
